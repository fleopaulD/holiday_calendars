# Changelog
Tous les changements notables apportés à ce projet seront documentés dans ce dossier.

Le format est basé sur [Keep a Changelog](http://keepachangelog.com/en/1.0.0/) et ce projet adhère à [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 0.2.1

### Fixed

- Modification du numéro de version

## 0.2.0

### Ajouté

- Support pour Nextcloud 25
- Ajout de deux nouvelles langues (allemand et français) pour les données sur les jours de congés aux Pays-Bas
- Traduction en néerlandais
- Journal des modifications en français

### Modifié

- Ajout d'un intervalle de dates à côté de la sélection du pays dans le sélecteur de pays de jours de congés.

### Supprimé

- Support pour Nextcloud 24

## 0.1.0

### Ajouté

- Première publication
